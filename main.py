import telebot
from telebot.types import ReplyKeyboardMarkup, KeyboardButton
import sqlite3

# создание бота с помощью токена
bot = telebot.TeleBot('6023675786:AAGrBTXbEzfPSHG0Wul_rIXRNArt_I_meJo')

# главное меню
main_menu = ReplyKeyboardMarkup(resize_keyboard=True)
btn_info = KeyboardButton('Информация')
btn_form = KeyboardButton('Анкета')
main_menu.add(btn_info, btn_form)

# меню информации
info_menu = ReplyKeyboardMarkup(resize_keyboard=True)
btn_project = KeyboardButton('О проекте')
btn_contest = KeyboardButton('Страница конкурса')
btn_instruction = KeyboardButton('Инструкция участника')
btn_social = KeyboardButton('Соц сети')
btn_back = KeyboardButton('Вернуться в главное меню')
info_menu.add(btn_project, btn_contest, btn_instruction, btn_social, btn_back)

#Меню соцсети
Soc_menu = ReplyKeyboardMarkup(resize_keyboard=True)
btn_Discord = KeyboardButton('Discord')
btn_Telegram = KeyboardButton('Telegram')
btn_VK = KeyboardButton('VK')
Soc_menu.add(btn_Discord, btn_Telegram, btn_VK, btn_back)

#Меню анкеты
Anketa_menu = ReplyKeyboardMarkup(resize_keyboard=True)
btn_AddZayavk = KeyboardButton('Оставить заявку')
btn_DeliteZayavk = KeyboardButton('Удалить заявку')
btn_ShowZayavk = KeyboardButton('Посмотреть заявки')
btn_ShowAnleta = KeyboardButton('Моя анкета')
Anketa_menu.add(btn_AddZayavk, btn_DeliteZayavk, btn_ShowZayavk,btn_ShowAnleta, btn_back)

#Меню анкеты
Zayavka_menu = ReplyKeyboardMarkup(resize_keyboard=True)
btn_Search_team = KeyboardButton('Ищу команду')
btn_Search_men = KeyboardButton('Ищу человека')
Zayavka_menu.add(btn_Search_team, btn_Search_men)


#Меню анкеты
AnkRed_menu = ReplyKeyboardMarkup(resize_keyboard=True)
btn_Redact = KeyboardButton('Редактировать анкету')
AnkRed_menu.add(btn_Redact, btn_back)

#Меню анкеты
ChtoRed_menu = ReplyKeyboardMarkup(resize_keyboard=True)
btn_FIo = KeyboardButton('ФИО')
btn_Gruppa = KeyboardButton('ГРУППА')
btn_Progi = KeyboardButton('ПРОГРАММЫ')
btn_Rod = KeyboardButton('РОД ДЕЯТЕЛЬНОСТИ')
btn_Osebe = KeyboardButton('О СЕБЕ')
ChtoRed_menu.add(btn_FIo, btn_Gruppa, btn_Progi, btn_Rod, btn_Osebe,  btn_back)

#Меню анкеты
DelZa_menu = ReplyKeyboardMarkup(resize_keyboard=True)
btn_chel = KeyboardButton('Удалить заявку о поиске человека')
btn_comand = KeyboardButton('Удалить заявку в команду')
DelZa_menu.add(btn_chel, btn_comand, btn_back)

#Меню Посмотреть заявки
VievZa_menu = ReplyKeyboardMarkup(resize_keyboard=True)
btn_po = KeyboardButton('Посмотреть заявки о поиске человека')
btn_ko = KeyboardButton('Посмотреть заявки в команду')
VievZa_menu.add(btn_po, btn_ko, btn_back)



# соединение с базой данных
conn = sqlite3.connect('users.db', check_same_thread=False)
cursor = conn.cursor()

# создание таблицы пользователей, если ее еще нет
cursor.execute('''CREATE TABLE IF NOT EXISTS users
                  (id INTEGER PRIMARY KEY, chat_id INTEGER, full_name TEXT, Gryp TEXT, Program TEXT, Deyatelnost TEXT, hobbies TEXT, Search INTEGER )''')
conn.commit()

# создание таблицы заявок, если ее еще нет
cursor.execute('''CREATE TABLE IF NOT EXISTS zauavk
                  (id INTEGER PRIMARY KEY, uesr_id INTEGER, Tex TEXT)''')
conn.commit()


# обработка кнопки "Анкета"
@bot.message_handler(func=lambda message: message.text == 'Анкета')
def form_handler(message):
    chat_id = message.chat.id
    # проверяем, есть ли запись о пользователе в базе данных
    cursor.execute("SELECT * FROM users WHERE chat_id=?", (chat_id,))
    user = cursor.fetchone()
    if user is None:
        bot.send_message(chat_id, 'Введите ФИО')
        bot.register_next_step_handler(message, get_full_name)
    else:
        bot.send_message(message.chat.id, 'Выберите нужный пункт', reply_markup=Anketa_menu)

# функция для получения ФИО
def get_full_name(message):
    chat_id = message.chat.id
    full_name = message.text
    bot.send_message(chat_id, 'Введите вашу группу')
    bot.register_next_step_handler(message, get_gryp, full_name)

# функция для получения группы
def get_gryp(message, full_name):
    chat_id = message.chat.id
    gryp = message.text
    bot.send_message(chat_id, 'Напишите какими программами вы пользуетесь')
    bot.register_next_step_handler(message, get_prog, full_name, gryp)

# функция для получения программ
def get_prog(message, full_name, gryp):
    chat_id = message.chat.id
    prog = message.text
    bot.send_message(chat_id, 'Опишите Ваш род деятельности')
    bot.register_next_step_handler(message, get_rod, full_name, gryp, prog)

# функция для получения Рода деятельности
def get_rod(message, full_name, gryp, prog):
    chat_id = message.chat.id
    rod = message.text
    bot.send_message(chat_id, 'Кратко напишите о себе. Что вы любите или не любите, хобби, интересы, Данная информация поможет лучше узнать о вас другим участникам')
    bot.register_next_step_handler(message, get_osebe, full_name, gryp, prog, rod)

# функция для получения О себе
def get_osebe(message, full_name, gryp, prog, rod):
    chat_id = message.chat.id
    osebe = message.text
    # добавляем данные в базу данных
    cursor.execute("INSERT INTO users(chat_id, full_name, Gryp, Program, Deyatelnost, hobbies, Search) VALUES(?, ?, ?, ?, ?, ?, ?)", (chat_id, full_name, gryp, prog, rod, osebe, 0 ))
    conn.commit()
    bot.send_message(chat_id, f'Спасибо за заполнение анкеты!\nВаше ФИО: {full_name}\nВаша группа: {gryp}\nВы владеете: {prog}\nВы увлекаетесь: {rod}\nИнформация о вас: {osebe}')



#Вывод кнопок заявки на поиск корманды или людей
@bot.message_handler(func=lambda message: message.text == 'Оставить заявку')
def Add_zayavk(message):
    bot.send_message(message.chat.id, 'Текст', reply_markup=Zayavka_menu)



#Заявка ищу человека
@bot.message_handler(func=lambda message: message.text == 'Ищу человека')
def get_Zayavk_text(message):
    chat_id = message.chat.id
    cursor.execute("SELECT * FROM zauavk WHERE uesr_id =?", (chat_id,))
    rows1 = cursor.fetchall()
    if len(rows1) > 3:
        bot.send_message(message.chat.id, 'Вы исчерпали лимит в 3 заявки. Для размещения новых удалите не актуальные заявки.', reply_markup=main_menu)
    else:
        bot.send_message(chat_id, 'Расскажите кто вам нужен')
        bot.register_next_step_handler(message, Add_zayavk, len(rows1))


#Заявка ищу человека 2
def Add_zayavk(message, kolv):
    chat_id = int(message.chat.id)
    Zaya_text = message.text
    cursor.execute("INSERT INTO zauavk(uesr_id, Tex) VALUES(?, ?)", (chat_id, Zaya_text))
    conn.commit()
    bot.send_message(message.chat.id, 'Ваша заявка оставлена. Вы потратили ' + str(kolv) + " из 3 возможных заявок", reply_markup=main_menu)




#Заявка ищу команду
@bot.message_handler(func=lambda message: message.text == 'Ищу команду')
def Set_serch_true(message):
    chat_id = int(message.chat.id)
    cursor.execute("UPDATE users SET Search = 1 WHERE chat_id =?", (chat_id,))
    conn.commit()
    bot.send_message(message.chat.id, 'Ваша заявка оставлена', reply_markup=main_menu)


#Заявка ищу команду
@bot.message_handler(func=lambda message: message.text == 'Моя анкета')
def Set_Moya_anketa(message):
    chat_id = int(message.chat.id)
    cursor.execute("SELECT * FROM users WHERE chat_id =?", (chat_id,))
    rows = cursor.fetchall()
    for row in rows:
        bot.send_message(chat_id, f'Ваше ФИО: {row[2]}\nВаша группа: {row[3]}\nВы владеете: {row[4]}\nВы увлекаетесь: {row[5]}\nИнформация о вас: {row[6]}')
    bot.send_message(message.chat.id, 'Ура', reply_markup=ChtoRed_menu)



#---------------------------------------------------------------------------------------------------------------------------------------------------
#Изменение анкеты
@bot.message_handler(func=lambda message: message.text == 'ФИО')
def Red_FIO(message):
    bot.send_message(message.chat.id, 'Введите ФИО')
    bot.register_next_step_handler(message, Red_FIOZ)


def Red_FIOZ(message):
    chat_id = int(message.chat.id)
    t5 = message.text
    cursor.execute("UPDATE users SET full_name = ? WHERE chat_id =?", (t5, chat_id))
    conn.commit()
    Set_Moya_anketa(message)
    bot.send_message(message.chat.id, 'Ваша анкета изменена')



@bot.message_handler(func=lambda message: message.text == 'ГРУППА')
def Red_grup(message):
    bot.send_message(message.chat.id, 'Введите ГРУППА')
    bot.register_next_step_handler(message, Red_grupZ)


def Red_grupZ(message):
    chat_id = int(message.chat.id)
    t4 = message.text
    cursor.execute("UPDATE users SET Gryp = ? WHERE chat_id =?", (t4, chat_id))
    conn.commit()
    Set_Moya_anketa(message)
    bot.send_message(message.chat.id, 'Ваша анкета изменена')



    #cursor.execute("INSERT INTO users(chat_id, full_name, Gryp, Program, Deyatelnost, hobbies, Search) VALUES(?, ?, ?, ?, ?, ?, ?)", (chat_id, full_name, gryp, prog, rod, osebe, 0 ))


@bot.message_handler(func=lambda message: message.text == 'ПРОГРАММЫ')
def Red_Prog(message):
    bot.send_message(message.chat.id, 'Введите ПРОГРАММЫ')
    bot.register_next_step_handler(message, Red_ProgZ)


def Red_ProgZ(message):
    chat_id = int(message.chat.id)
    t3 = message.text
    cursor.execute("UPDATE users SET Program = ? WHERE chat_id =?", (t3, chat_id))
    conn.commit()
    Set_Moya_anketa(message)
    bot.send_message(message.chat.id, 'Ваша анкета изменена')




@bot.message_handler(func=lambda message: message.text == 'РОД ДЕЯТЕЛЬНОСТИ')
def Red_Rod(message):
    bot.send_message(message.chat.id, 'Введите РОД ДЕЯТЕЛЬНОСТИ')
    bot.register_next_step_handler(message, Red_RodZ)


def Red_RodZ(message):
    chat_id = int(message.chat.id)
    t1 = message.text
    cursor.execute("UPDATE users SET Deyatelnost = ? WHERE chat_id =?", (t1, chat_id))
    conn.commit()
    Set_Moya_anketa(message)
    bot.send_message(message.chat.id, 'Ваша анкета изменена')




@bot.message_handler(func=lambda message: message.text == 'О СЕБЕ')
def Red_Osebe(message):
    bot.send_message(message.chat.id, 'Введите О СЕБЕ')
    bot.register_next_step_handler(message, Red_OsebeZ)


def Red_OsebeZ(message):
    chat_id = int(message.chat.id)
    t2 = message.text
    cursor.execute("UPDATE users SET hobbies = ? WHERE chat_id =?", (t2, chat_id))
    conn.commit()
    Set_Moya_anketa(message)
    bot.send_message(message.chat.id, 'Ваша анкета изменена')

#---------------------------------------------------------------------------------------------------------------------------------------------------




#удалить заявку меню
@bot.message_handler(func=lambda message: message.text == 'Удалить заявку')
def Del_zayavk(message):
    bot.send_message(message.chat.id, 'Ваша заявка удалена', reply_markup=DelZa_menu)

#удалить заявку в команду
@bot.message_handler(func=lambda message: message.text == 'Удалить заявку в команду')
def Del_zayavk(message):
    chat_id = int(message.chat.id)
    cursor.execute("SELECT * FROM users WHERE chat_id=? AND Search =1", (chat_id,))
    user1 = cursor.fetchone()
    try:
        if user1[7] == 1:
            cursor.execute("UPDATE users SET Search = 0 WHERE chat_id =?", (chat_id,))
            conn.commit()
            bot.send_message(message.chat.id, 'Ваша заявка удалена', reply_markup=main_menu)
    except:
            bot.send_message(message.chat.id, 'У вас нет активной заявки', reply_markup=main_menu)




#удалить заявку в команду
@bot.message_handler(func=lambda message: message.text == 'Удалить заявку о поиске человека')
def Del_zayavk(message):
    chat_id = int(message.chat.id)
    cursor.execute("SELECT * FROM zauavk WHERE uesr_id=?", (chat_id,))
    user2 = cursor.fetchmany(3)
    print(user2)
    sc = 0
    for w in user2:
        sc = sc + 1
        print(w[2])
        bot.send_message(message.chat.id, str(sc) + ":")
        bot.send_message(message.chat.id, w[2])
    bot.send_message(message.chat.id, "Выберите номер заявки, которую хотите удалить")
    bot.register_next_step_handler(message, del_za, user2)

def del_za(message, dat):
    s = message.text
    sc = 0
    try:
        if int(s) > int(len(dat)):
            bot.send_message(message.chat.id, "Вы можете ввести только число от 1 до " + str(len(dat)))
            bot.register_next_step_handler(message, del_za, dat)
        for w in dat:
            sc = sc + 1
            print(sc)
            print(s)
            if sc == int(s):
                dp = int(w[0])
                cursor.execute("DELETE from zauavk where id = ?", (dp,))
                print("Удалил запись с ID" + str(dp))
                conn.commit()
                bot.send_message(message.chat.id, "Заявка удалена", reply_markup=Anketa_menu)
    except:
        bot.send_message(message.chat.id, "Вы можете ввести только число от 1 до " + str(len(dat)))
        bot.register_next_step_handler(message, del_za, dat)



VievZa_menu




@bot.message_handler(func=lambda message: message.text == 'Посмотреть заявки')
def v_zayavk(message):
    bot.send_message(message.chat.id, "Выберите", reply_markup= VievZa_menu)




@bot.message_handler(func=lambda message: message.text == 'Посмотреть заявки о поиске человека')
def Viev_zayavk(message):
    chat_id = int(message.chat.id)
    cursor.execute("SELECT * FROM zauavk")
    user2 = cursor.fetchmany(300)
    print(user2)
    sc = 0
    for w in user2:
        sc = sc + 1
        print(w[2])
        bot.send_message(message.chat.id, str(sc) + ":")
        bot.send_message(message.chat.id, w[2])

@bot.message_handler(func=lambda message: message.text == 'Посмотреть заявки в команду')
def Viev_zayavk(message):
    chat_id = int(message.chat.id)
    cursor.execute("SELECT * FROM users WHERE Search =1")
    user2 = cursor.fetchmany(300)
    if len(user2) > 0:
        print(user2)
        sc = 0
        for w in user2:
            sc = sc + 1
            bot.send_message(chat_id, f'ФИО: {w[2]}\nГруппа: {w[3]}\nВладеетет: {w[4]}\nУвлекается: {w[5]}\nО себе: {w[6]}')
    else:
        bot.send_message(chat_id, "Нет активных заявок")
        print("Else")


# обработка команды /start
@bot.message_handler(commands=['start'])
def start_handler(message):
    bot.send_message(message.chat.id, 'Привет! Это тестовый бот. Выберите пункт меню', reply_markup=main_menu)



# обработка кнопки "Информация"
@bot.message_handler(func=lambda message: message.text == 'Информация')
def info_handler(message):
    bot.send_message(message.chat.id, 'Выберите нужный пункт', reply_markup=info_menu)

# обработка кнопки "О проекте"
@bot.message_handler(func=lambda message: message.text == 'О проекте')
def project_handler(message):
    bot.send_message(message.chat.id, 'Информация о проекте')

# обработка кнопки "Страница конкурса"
@bot.message_handler(func=lambda message: message.text == 'Страница конкурса')
def contest_handler(message):
    bot.send_message(message.chat.id, 'Страница конкурса')

# обработка кнопки "Инструкция участника"
@bot.message_handler(func=lambda message: message.text == 'Инструкция участника')
def instruction_handler(message):
    bot.send_message(message.chat.id, 'Инструкция участника')

# обработка кнопки "Соц сети"
@bot.message_handler(func=lambda message: message.text == 'Соц сети')
def social_handler(message):
    bot.send_message(message.chat.id, 'Соц сети', reply_markup=Soc_menu)

# обработка кнопки "Вернуться в главное меню"
@bot.message_handler(func=lambda message: message.text == 'Вернуться в главное меню')
def back_handler(message):
    bot.send_message(message.chat.id, 'Выберите пункт меню', reply_markup=main_menu)

# запуск бота
bot.polling()
